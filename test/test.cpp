#define CATCH_CONFIG_MAIN

#include "field/player_field/player_field.hpp"
#include "../catch.hpp"


#include "field/basic_field.hpp"


TEST_CASE("test field validity check function")
{
    auto fillShipInfo = BasicField::fillShipInfo;

    bool valid = false;

    SECTION("invalid fields")
    {
        SECTION("water only")
        {
            auto water_only = BasicField();

            auto ship_info = fillShipInfo(water_only, true, valid);

            REQUIRE(ship_info.empty());
            REQUIRE(not valid);
        }

        SECTION("bad placement")
        {
            std::vector<std::string> field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~#~~~~~", // 2
                    "###~~~~##~", // 3
                    "~~~~~~~~~~", // 4
                    "#~#~~~#~~~", // 5
                    "#~~~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "~~~~~~~~~~", // 8
                    "~~~~~~~~~~"  // 9
            }; // E 2

            REQUIRE_THROWS(PlayerField(field));

            field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~~~~~~~", // 2
                    "###~~~~~#~", // 3
                    "~~~##~~~~~", // 4
                    "#~~~~~#~~~", // 5
                    "#~~~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "~~~~~~~~~~", // 8
                    "~#~~~~~~~~"  // 9
            }; // D 4

            REQUIRE_THROWS(PlayerField(field));

            field = {
                    //       ABCDEFGHIJ
                    "~~#~~~~~~~", // 0
                    "####~~~~~~#", // 1
                    "~~#~~~~~~~", // 2
                    "###~~~~~#~", // 3
                    "~~~##~~~~~", // 4
                    "#~~~~~#~~~", // 5
                    "#~~~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "~~~~~~~~~~", // 8
                    "~#~~~~~~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));
        }

        SECTION("bad number of ship cells")
        {
            std::vector<std::string> field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~#~~~~~", // 2
                    "###~~~~##~", // 3
                    "~~~~~~~~~~", // 4
                    "#~#~~~#~~~", // 5
                    "#~~~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "~~~~~~###~", // 8
                    "~~~~~~~~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));

            field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~~~~~~~", // 2
                    "~~~~~~~~~#~", // 3
                    "~~~##~~~~~", // 4
                    "#~~~~~~~~~", // 5
                    "#~~~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "~~~~~~~~~~", // 8
                    "~#~~~~~~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));
        }

        SECTION("invalid chars")
        {
            std::vector<std::string> field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~#~~~~~", // 2
                    "###~~~~##~", // 3
                    "~~*~~~~~~~", // 4
                    "#~#~~~#~~~", // 5
                    "#~~~#~~*~~", // 6
                    "~~~~#~~~~~", // 7
                    "~*~~~~###~", // 8
                    "~~~~o~~~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));

            field = {
                    //       ABCDEFGHIJ
                    "~~~!!~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~~~~~~~", // 2
                    "~~~~~~%~~#~", // 3
                    "~~~##~~~~~", // 4
                    "#~~~~~~~~~", // 5
                    "#~(~#~~~~~", // 6
                    "~~~~#~~~~~", // 7
                    "yellow sub", // 8
                    "marine~~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));
        }

        SECTION("bad number of lines / characters")
        {
            std::vector<std::string> field = {
                    //       ABCDEFGHIJ
                    "~~~~~~~~~~", // 0
                    "####~###~#", // 1
                    "~~~~#~~~~~", // 2
                    "###~~~~##~", // 3
                    "~~*~~~~~~", // 4
                    "#~#~~~#~~~", // 5
                    "#~~~#~~*~~", // 6
                    "~~~~#~~~~~", // 7
                    "~*~~~~###~~~"  // 9
            };

            REQUIRE_THROWS(PlayerField(field));
        }
    }

    SECTION("some valid field")
    {
        auto player_field = PlayerField();
        std::vector<std::string> field = {
                //       ABCDEFGHIJ
                "~~~~~~~~~~", // 0
                "####~###~#", // 1
                "~~~~~~~~~~", // 2
                "###~~~~##~", // 3
                "~~~~~~~~~~", // 4
                "#~#~~~#~~~", // 5
                "#~~~#~~~~~", // 6
                "~~~~#~~~~~", // 7
                "~~~~~~~#~~", // 8
                "~~~~~~~~~~"  // 9
        };

        REQUIRE_NOTHROW(player_field = PlayerField(field));

        auto ships_info = player_field.getShipsInfo();

        for (auto const & item : ships_info) // i'd rather die than spend time on writing test for this
        {
            std::cout << item.first << " : ";

            for (auto const & position : *item.second)
            {
                std::cout << position << "  ";
            }

            std::cout << std::endl;
        }

        std::cout << std::endl;

        field = {
                //       ABCDEFGHIJ
                "~~~~~~~~~~", // 0
                "~#~#~#~#~~", // 1
                "~#~#~#~~~~", // 2
                "~#~#~~~#~~", // 3
                "~#~~~#~#~~", // 4
                "~~~#~#~#~~", // 5
                "~~~~~~~~~~", // 6
                "~~~~~~##~~", // 7
                "~~~#~~~~~~", // 8
                "~~~~~~~#~~"  // 9
        };

        REQUIRE_NOTHROW(player_field = PlayerField(field));

        ships_info = player_field.getShipsInfo();

        for (auto const & item : ships_info)
        {
            std::cout << item.first << " : ";

            for (auto const & position : *item.second)
            {
                std::cout << position << "  ";
            }

            std::cout << std::endl;
        }
    }
}
