#include "server.hpp"
#include "player/process_wrapper/process_wrapper.hpp"
#include "utility/cppncurses/cppcurses.hpp"


static inline void displayFields(
        BasicField const & f1,
        BasicField const & f2)
{
    auto & terminal = Curses::getInstance();

    terminal.displayRepresentable(0, 5, f1);
    terminal.displayRepresentable(16, 5, f2);
}


static inline void displayNames(
        std::string const & n1,
        std::string const & n2)
{
    auto & terminal = Curses::getInstance();

    if (n1.size() >= 16)
    {
        auto cn1 = n1.substr(0, 11) + "...";
        terminal.putString(0, 4, cn1);
    }
    else
    {
        terminal.putString(0, 4, n1);
    }

    terminal.putString(16, 4, n2);
}


static inline void displayWinner(
        std::string const & n1)
{
    auto & terminal = Curses::getInstance();

    terminal.putString(0, 20, n1 + " has won the game!", Curses::Attribute::Bold);
}


static inline void displayKillCount(
        size_t kills1,
        size_t kills2,
        size_t x1,
        size_t x2)
{
    auto & terminal = Curses::getInstance();

    terminal.putString(x1, 17, std::to_string(kills1));
    terminal.putString(x2, 18, std::to_string(kills2));
}


std::pair<std::string, std::string> server::makeNames(
        std::string const & exec1,
        std::string const & exec2)
{
    std::string exec_name1 = exec1;
    std::string exec_name2 = exec2;

    size_t s_index = exec1.rfind('/');

    if (s_index != std::string::npos)
    {
        exec_name1 = exec1.substr(s_index + 1, exec1.size());
    }

    s_index = exec2.rfind('/');

    if (s_index != std::string::npos)
    {
        exec_name2 = exec2.substr(s_index + 1, exec2.size());
    }

    if (exec_name1 == exec_name2 and exec1 == exec2)
    {
        exec_name1.append(" (1)");
        exec_name2.append(" (2)");
    }

    return {exec_name1, exec_name2};
}


static inline void termSetup()
{
    auto & terminal = Curses::getInstance();

    terminal.setCursor(Curses::Visibility::Invisible);
    terminal.setAutoRefresh(false);

    terminal.setCharProperties('X', Curses::Attribute::Bold, Curses::Colour::Red);
    terminal.setCharProperties('~', Curses::Attribute::Bold, Curses::Colour::Blue);

    terminal.setCharProperties(
            '#',
            Curses::combine(
                    Curses::Attribute::Bold,
                    Curses::Attribute::Dim
            ),
            Curses::Colour::White
    );
}


std::pair<std::string, std::string> server::runBattle(
        std::string const & exec1,
        std::string const & exec2,
        std::chrono::milliseconds const & delay,
        std::chrono::milliseconds const & wait_between_starting_exec,
        std::string const &title)
{
    termSetup();

    auto & terminal = Curses::getInstance();

    terminal.putString(0, 0, title);

    auto const names = makeNames(exec1, exec2);

    terminal.putString(0, 17, names.first + " kill count: ");
    terminal.putString(0, 18, names.second + " kill count: ");

    displayKillCount(
            0, 0,
            names.first.size() + 14,
            names.second.size() + 14
    );

    displayNames(names.first, names.second);

    terminal.refresh();

    std::pair<std::string, std::string> winner;

    std::unique_ptr<ProcessWrapper> player1;
    std::unique_ptr<ProcessWrapper> player2;

    try
    {
        player1 = std::make_unique<ProcessWrapper>(names.first, exec1);

        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        if (not player1->getProcess().alive())
        {
            throw std::runtime_error("process terminated");
        }
    }
    catch (std::exception & e)
    {
        terminal.exitCursesMode();

        std::cerr << "\nFailed to spawn process for \"" << exec1 << "\": " << e.what() << std::endl;

        exit(EXIT_FAILURE);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(wait_between_starting_exec));

    try
    {
        player2 = std::make_unique<ProcessWrapper>(names.second, exec2);

        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        if (not player2->getProcess().alive())
        {
            throw std::runtime_error("process terminated");
        }
    }
    catch (std::exception & e)
    {
        terminal.exitCursesMode();

        std::cerr << "\nFailed to spawn process for \"" << exec2 << "\": " << e.what() << std::endl;

        exit(EXIT_FAILURE);
    }

    ProcessWrapper * this_player = player1.get();
    ProcessWrapper * other_player = player2.get();

    Player::DamageDealt damage_dealt;
    BasicField::Position pos;

    size_t niter = 0;

    std::ostringstream os;

    try
    {
        player1->arrange();
    }
    catch (std::runtime_error & e)
    {
        terminal.exitCursesMode();

        std::cerr << std::endl << e.what() << std::endl;

        player1->sigLose();
        player2->sigWin();

        winner = {player2->name, player2->executable_name};

        goto cleanup;
    }

    try
    {
        player2->arrange();
    }
    catch (std::runtime_error & e)
    {
        terminal.exitCursesMode();

        std::cerr << std::endl << e.what() << std::endl;

        player1->sigWin();
        player2->sigLose();

        winner = {player1->name, player1->executable_name};

        goto cleanup;
    }

    for (; niter < 500; ++niter)
    {
        try
        {
            do
            {
                try
                {
                    pos = this_player->fire();
                }
                catch (std::runtime_error & e)
                {
                    terminal.exitCursesMode();

                    std::cerr << std::endl << e.what() << std::endl;

                    other_player->sigWin();
                    this_player->sigLose();

                    std::cerr << other_player->name << " has won the game!" << std::endl;

                    winner = {other_player->name, other_player->executable_name};

                    goto cleanup;
                }

                os.str("");
                os << this_player->name << " fires at " << pos << ": ";
                terminal.clearLine(2);
                terminal.putString(0, 2, os.str());

                auto curs_pos = terminal.getCursorPosition();

                try
                {
                    damage_dealt = other_player->processIncomingDamage(pos);
                }
                catch (std::runtime_error & e)
                {
                    terminal.exitCursesMode();

                    std::cerr << std::endl << e.what() << std::endl;

                    this_player->sigWin();
                    other_player->sigLose();

                    std::cerr << this_player->name << " has won the game!" << std::endl;

                    winner = {this_player->name, this_player->executable_name};

                    goto cleanup;
                }

                try
                {
                    this_player->processDamageDealt(damage_dealt);
                }
                catch (std::runtime_error & e)
                {
                    terminal.exitCursesMode();

                    std::cerr << std::endl << e.what() << std::endl;

                    other_player->sigWin();
                    this_player->sigLose();

                    std::cerr << other_player->name << " has won the game!" << std::endl;

                    winner = {other_player->name, other_player->executable_name};

                    goto cleanup;
                }

                os.str("");
                os << damage_dealt << "!";
                terminal.putString(curs_pos.x, curs_pos.y, os.str());

                displayFields(player1->getField(), player2->getField());

                displayKillCount(
                        player1->getKillCount(),
                        player2->getKillCount(),
                        names.first.size() + 14,
                        names.second.size() + 14
                );

                terminal.refresh();

                if (damage_dealt == Player::DamageDealt::Kill)
                {
                    if (this_player->getKillCount() == 10)
                    {
                        this_player->sigWin();
                        other_player->sigLose();

                        displayWinner(this_player->name);

                        terminal.refresh();

                        winner = {this_player->name, this_player->executable_name};

                        goto cleanup;
                    }
                }

                std::this_thread::sleep_for(delay);
            }
            while (damage_dealt == Player::DamageDealt::Hit or damage_dealt == Player::DamageDealt::Kill);
        }
        catch (game_over & e)
        {
            throw std::runtime_error("Game has ended without anyone scoring 10 kills. An error must have occurred.");
        }

        std::swap(this_player, other_player);
    }

    if (niter == 500)
    {
        terminal.exitCursesMode();

        std::cerr << "Abnormal turns count (>= 500)" << std::endl;
    }

    cleanup:

    std::this_thread::sleep_for(std::chrono::milliseconds(15));

    if (not player1->getProcess().alive() and not player2->getProcess().alive())
    {
        return winner;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(965));

    if (player1->getProcess().alive())
    {
        terminal.exitCursesMode();

        std::cerr << player1->name << " (" << player1->executable_name << ")"
                    << " didn't terminate in 1 second" << std::endl;
        player1->getProcess().terminate();
    }

    if (player2->getProcess().alive())
    {
        terminal.exitCursesMode();

        std::cerr << player2->name << " (" << player2->executable_name << ")"
                    << " didn't terminate in 1 second" << std::endl;
        player2->getProcess().terminate();
    }

    return winner;
}
