#ifndef BATTLESHIP_PROCESS_WRAPPER_HPP
#define BATTLESHIP_PROCESS_WRAPPER_HPP


#include <utility>


#include "player/player.hpp"
#include "utility/subprocess/subprocess/subprocess.hpp"
#include "field/basic_field.hpp"
#include "field/player_field/player_field.hpp"


class ProcessWrapper : public Player
{
public:
    explicit ProcessWrapper(
            std::string name,
            std::string executable_name);

    void arrange();

    DamageDealt processIncomingDamage(BasicField::Position position) override;

    void processDamageDealt(DamageDealt damage) override;

    BasicField::Position fire() override;

    BasicField const & getField() const override;

    BasicField const & getEnemyField() const override;

    subprocess::Subprocess & getProcess();

    size_t getKillCount() const;

    void sigWin() noexcept;

    void sigLose() noexcept;

private:
    BasicField::Position last_pos;
    Player::DamageDealt last_damage_dealt;
    BasicField enemy_field;
    PlayerField field;
    size_t ships_left;
    size_t kill_count;

    subprocess::Subprocess player_process;

public:
    std::string const name;
    std::string const executable_name;
};


#endif //BATTLESHIP_PROCESS_WRAPPER_HPP
