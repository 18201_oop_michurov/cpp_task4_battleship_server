#include <regex>
#include <fstream>


#include "process_wrapper.hpp"


ProcessWrapper::ProcessWrapper(
        std::string name,
        std::string executable_name)
        :
        last_damage_dealt(Player::DamageDealt::Miss),
        last_pos({'A', 0}),
        enemy_field(BasicField::Cell::FogOfWar),
        player_process(subprocess::spawn(executable_name.c_str(), executable_name.c_str(), nullptr)),
        name(std::move(name)),
        executable_name(std::move(executable_name)),
        ships_left(4 + 3 * 2 + 2 * 3 + 1 * 4),
        kill_count(0)
{
    signal(SIGPIPE, SIG_IGN);
}


void ProcessWrapper::arrange()
{
    std::string buff;
    std::vector<std::string> lines;
    std::ostringstream err;

    try
    {
        this->player_process.std_in << "Arrange!" << std::endl;
    }
    catch (io::write_error & e)
    {
        err << this->name << ": write error" << std::endl;
        throw std::runtime_error(err.str());
    }

    lines.reserve(BasicField::FieldSize);

    for (size_t i = 0; i < BasicField::FieldSize; ++i)
    {
        io::getline(this->player_process.std_out, buff);

        if (this->player_process.std_out.eof())
        {
            err << this->name << ": unexpected eof" << std::endl;
            throw std::runtime_error(err.str());
        }
        if (this->player_process.std_out.timeout_expired())
        {
            err << this->name << " didn't respond to \"Arrange!\" in 1 second" << std::endl;
            throw std::runtime_error(err.str());
        }
        if (buff.empty())
        {
            err << this->name << ": bad number of lines in response to \"Arrange!\" (expected 10)" << std::endl;
            throw std::runtime_error(err.str());
        }
        if (buff.size() != BasicField::FieldSize)
        {
            err << this->name << ": bad number of characters in a line in response to \"Arrange!\" (expected 10)"
                << std::endl;
            throw std::runtime_error(err.str());
        }

        lines.emplace_back(std::move(buff));
    }

    // check field validity

    std::for_each(
            lines.begin(),
            lines.end(),
            [](std::string & s)
            {
                std::replace(s.begin(), s.end(), '1', static_cast<char>(BasicField::Cell::Ship));
                std::replace(s.begin(), s.end(), '0', static_cast<char>(BasicField::Cell::Water));
            }
    );

    try
    {
        this->field = PlayerField(lines);
    }
    catch (std::runtime_error & e)
    {
        err << this->name << ": field is invalid" << std::endl;
        throw std::runtime_error(err.str());
    }

    this->ships_left = 20;
}


Player::DamageDealt ProcessWrapper::processIncomingDamage(BasicField::Position position)
{
    std::string damage;
    std::ostringstream err;

    // std::cerr << this->name << " calculated damage suffered at server side" << std::endl;

    if (this->ships_left == 0)
    {
        err << this->name << ": no ships left!" << std::endl;
        throw game_over(err.str());
        //throw game_over("Game has ended!");
    }

    if (this->field.getShipsInfo().empty())
    {
        err << this->name << ": no ships left!" << std::endl;
        throw game_over(err.str());
        //throw game_over("Game has ended!");
    }

    if (this->field[position] != BasicField::Cell::Ship)
    {
        this->field[position] = BasicField::Cell::Miss;
        return Player::DamageDealt::Miss;
    }

    auto ship_info = this->field.getShipsInfo().find(position);

    if (ship_info == this->field.getShipsInfo().end() or ship_info->second.get()->empty())
    {
        err << this->name << ": BasicField data has been corrupted or something" << std::endl;
        throw std::runtime_error(err.str());
    }

    this->field[position] = BasicField::Cell::Hit;

    auto ship_cells = ship_info->second.get();

    ship_cells->erase(
            std::find(
                    ship_cells->begin(),
                    ship_cells->end(),
                    position));

    if (ship_cells->empty())
    {
        this->ships_left -= 1;

        return Player::DamageDealt::Kill;
    }

    return Player::DamageDealt::Hit;
/*
    try
    {
        this->player_process.std_in << "Enemy shooted into " << position << std::endl;
    }
    catch (io::write_error & e)
    {
        err << this->name << ": write error";

        if (this->player_process.alive())
        {
            err << " (process is still running)";
        }
        else
        {
            err << " (process terminated)";
        }
        err << std::endl;

        throw std::runtime_error(err.str());
    }

    io::getline(this->player_process.std_out, damage);

    if (damage == "Hit")
    {
        this->field[position] = BasicField::Cell::Hit;
        return Player::DamageDealt::Hit;
    }
    else if (damage == "Miss")
    {
        this->field[position] = BasicField::Cell::Miss;
        return Player::DamageDealt::Miss;
    }
    else if (damage == "Kill")
    {
        this->field[position] = BasicField::Cell::Hit;
        return Player::DamageDealt::Kill;
    }
    else if (this->player_process.std_out.eof())
    {
        err << this->name << ": unexpected eof" << std::endl;
        throw std::runtime_error(err.str());
    }
    else if (this->player_process.std_out.timeout_expired() or damage.empty())
    {
        pass;
        err << this->name << ": bad data format in response to \"Enemy shooted into " << position << "\" "
            "(expected \"Hit\", \"Miss\" or \"Kill\", got none)" << std::endl;
        throw std::runtime_error(err.str());
    }
    else
    {
        err << this->name << ": bad data format in response to \"Enemy shooted into " << position << "\" "
            "(expected \"Hit\", \"Miss\", \"Kill\" or none, got \"" << damage << "\")" << std::endl;
        throw std::runtime_error(err.str());
    }*/
}


void ProcessWrapper::processDamageDealt(DamageDealt damage)
{
    this->last_damage_dealt = damage;

    std::ostringstream err;

    try
    {
        this->player_process.std_in << damage << std::endl;
    }
    catch (io::write_error & e)
    {
        err << this->name << ": write error";

        if (this->player_process.alive())
        {
            err << " (process is still running)";
        }
        else
        {
            err << " (process terminated)";
        }
        err << std::endl;

        throw std::runtime_error(err.str());
    }

    switch (damage)
    {
        case DamageDealt::Hit:
            this->enemy_field[this->last_pos] = BasicField::Cell::Hit;
            break;
        case DamageDealt::Kill:
            this->enemy_field[this->last_pos] = BasicField::Cell::Hit;
            ++this->kill_count;
            break;
        case DamageDealt::Miss:
            this->enemy_field[this->last_pos] = BasicField::Cell::Miss;
            break;
    }
}


BasicField::Position ProcessWrapper::fire()
{
    std::ostringstream err;

    if (this->last_damage_dealt == Player::DamageDealt::Miss)
    {
        try
        {
            this->player_process.std_in << "Shoot!" << std::endl;
        }
        catch (io::write_error & e)
        {
            err << this->name << ": write error";

            if (this->player_process.alive())
            {
                err << " (process is still running)";
            }
            else
            {
                err << " (process terminated)";
            }
            err << std::endl;

            throw std::runtime_error(err.str());
        }
    }

    std::regex regex("^([A-J]) ([0-9])$");
    std::cmatch match;

    std::string buff;

    io::getline(this->player_process.std_out, buff);

    if (this->player_process.std_out.eof())
    {
        err << this->name << ": unexpected eof" << std::endl;
        throw std::runtime_error(err.str());
    }
    if (this->player_process.std_out.timeout_expired())
    {
        if (this->last_damage_dealt == Player::DamageDealt::Miss)
        {
            err << this->name << " didn't respond to \"Shoot!\" in 1 second" << std::endl;
        }
        else
        {
            err << this->name << " didn't fire again in 1 second" << std::endl;
        }
        throw std::runtime_error(err.str());
    }
    if (buff.empty())
    {
        if (this->last_damage_dealt == Player::DamageDealt::Miss)
        {
            err << this->name << ": empty response to \"Shoot!\"" << std::endl;
        }
        else
        {
            err << this->name << ": empty response (expected field position)" << std::endl;
        }
        throw std::runtime_error(err.str());
    }

    std::regex_match(buff.c_str(), match, regex);

    if (match.empty())
    {
        if (this->last_damage_dealt == Player::DamageDealt::Miss)
        {
            err << this->name << R"(: bad data format in response to "Shoot!" (expected field position, got ")"
                << buff <<"\")" << std::endl;
        }
        else
        {
            err << this->name << ": bad data format (expected field position, got \""
                << buff <<"\")" << std::endl;
        }
        throw std::runtime_error(err.str());
    }

    char x = buff[0];
    char y = buff[2];

    BasicField::Position position = {static_cast<unsigned char>(x), static_cast<unsigned char>(y - '0')};

    if (this->enemy_field[position] != BasicField::Cell::FogOfWar)
    {
        err << this->name << " already fired at position " << position << std::endl;
        throw std::runtime_error(err.str());
    }

    this->last_pos = position;

    return position;
}


BasicField const & ProcessWrapper::getField() const
{
    return this->field;
}


BasicField const & ProcessWrapper::getEnemyField() const
{
    return this->enemy_field;
}


subprocess::Subprocess & ProcessWrapper::getProcess()
{
    return this->player_process;
}


void ProcessWrapper::sigWin() noexcept
{
    try
    {
        this->player_process.std_in << "Win!" << std::endl;
    }
    catch (io::write_error & e)
    {
        std::cerr << this->name << ": write error";

        if (this->player_process.alive())
        {
            std::cerr << " (process is still running)";
        }
        else
        {
            std::cerr << " (process terminated)";
        }
        std::cerr << std::endl;
    }
}


void ProcessWrapper::sigLose() noexcept
{
    try
    {
        this->player_process.std_in << "Lose" << std::endl;
    }
    catch (io::write_error & e)
    {
        std::cerr << this->name << ": write error";

        if (this->player_process.alive())
        {
            std::cerr << " (process is still running)";
        }
        else
        {
            std::cerr << " (process terminated)";
        }
        std::cerr << std::endl;
    }
}


size_t ProcessWrapper::getKillCount() const
{
    return this->kill_count;
}
