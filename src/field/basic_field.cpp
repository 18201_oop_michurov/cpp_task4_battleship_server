#include <numeric>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <unordered_map>


#include "basic_field.hpp"


BasicField::ShipInfo BasicField::fillShipInfo(
        BasicField const & field,
        bool fill_info,
        bool & is_valid)
{
    if (field.getLines().size() != BasicField::FieldSize)
    {
        is_valid = false;
        return {};
    }

    for (auto const & line : field.getLines())
    {
        if (line.size() != BasicField::FieldSize)
        {
            is_valid = false;
            return {};
        }
    }

    BasicField::ShipInfo info_map;

    auto copy = field;

    std::vector<BasicField::Position> unchecked_positions;

    cell_count_t total = 0;

    auto reset_unchecked = [&copy, &unchecked_positions]() -> void
    {
        unchecked_positions.clear();

        for (unsigned char x = 'A'; x < 'A' + BasicField::FieldSize; ++x)
        {
            for (unsigned char y = 0; y < BasicField::FieldSize; ++y)
            {
                if (copy[{x, y}] != BasicField::Cell::NearShip)
                {
                    unchecked_positions.emplace_back(x, y);
                }
            }
        }
    };

    reset_unchecked();

    std::unordered_map<BasicField::Cell, cell_count_t> cells_count = {};

    for (auto const & position : unchecked_positions)
    {
        cells_count[static_cast<BasicField::Cell>(copy[position])]++;
    }

    if (unchecked_positions.size() != 100
        or cells_count[BasicField::Cell::Water] != 80
        or cells_count[BasicField::Cell::Ship] != 20)
    {
        is_valid = false;
        return {};
    }

    std::unordered_map<cell_count_t, size_t> ships_count = {};

    auto detect_ship = [
            &copy] (BasicField::Position const & p)
    {
        std::vector<BasicField::Position> cells;

        cell_count_t dx = BasicField::Position::isValid(p.x + 1, p.y)
                          and copy[{static_cast<unsigned char>(p.x + 1), p.y}] == BasicField::Cell::Ship;
        cell_count_t dy = BasicField::Position::isValid(p.x, p.y + 1)
                          and copy[{p.x, static_cast<unsigned char>(p.y + 1)}] == BasicField::Cell::Ship;

        bool is_horizontal = dx;

        unsigned char x = p.x;
        unsigned char y = p.y;

        if (dx)
        {
            while (BasicField::Position::isValid(x, p.y) and copy[{x, p.y}] == BasicField::Cell::Ship)
            {
                cells.emplace_back(x, p.y);
                ++x;
            }
        }
        else if (dy)
        {
            while (BasicField::Position::isValid(p.x, y) and copy[{p.x, y}] == BasicField::Cell::Ship)
            {
                cells.emplace_back(p.x, y);
                ++y;
            }
        }
        else
        {
            cells.emplace_back(p.x, p.y);
        }

        struct
        {
            std::vector<BasicField::Position> ship_cells;
            bool is_horizontal;
        } result = {cells, is_horizontal};

        return result;
    };

    do
    {
        auto it = unchecked_positions.begin();
        for (; it != unchecked_positions.end(); ++it)
        {
            if (field[*it] == BasicField::Ship)
            {
                break;
            }
        }

        if (it == unchecked_positions.end())
        {
            if (total != 20)
            {
                is_valid = false;
                return {};
            }
            else
            {
                break;
            }
        }

        auto result = detect_ship(*it);

        auto surroundings = copy.getSurroundings(result.ship_cells.size(), result.is_horizontal, *it);

        if (std::count_if(
                surroundings.begin(),
                surroundings.end(),
                [&copy](BasicField::Position const & p)
                {
                    return copy[p] == BasicField::Cell::Ship;
                }) != 0)
        {
            is_valid = false;
            return {};
        }
        else
        {
            total += result.ship_cells.size();

            for (auto const & position : surroundings)
            {
                copy[position] = BasicField::Cell::NearShip;
            }

            for (auto const & position : result.ship_cells)
            {
                copy[position] = BasicField::Cell::NearShip;
            }

            ships_count[result.ship_cells.size()] += 1;

            if (fill_info)
            {
                auto p = std::make_shared<std::vector<BasicField::Position>>(result.ship_cells);

                for (auto const & position : result.ship_cells)
                {
                    info_map[position] = p;
                }
            }
        }

        reset_unchecked();
    }
    while (not unchecked_positions.empty());

    if (ships_count[4] != 1 or ships_count[3] != 2 or ships_count[2] != 3 or ships_count[4] != 1)
    {
        is_valid = false;
        return {};
    }

    is_valid = true;
    return info_map;
}


BasicField::BasicField(
        Cell filler_value)
        :
        field(std::move(
                std::vector<std::string>(
                        BasicField::FieldSize,
                        std::string(BasicField::FieldSize, filler_value))))
{}


BasicField::BasicField(
        std::vector<std::string> field)
        :
        field(std::move(field))
{
    bool valid = true;
    BasicField::fillShipInfo(*this, false, valid);

    if (not valid)
    {
        throw std::runtime_error("Error: invalid field");
    }
}


std::string BasicField::toString(
        bool bin,
        bool axis) const
{
    auto copy = this->field;

    if (bin)
    {
        std::for_each(
                copy.begin(),
                copy.end(),
                [](std::string & s)
                {
                    std::replace(s.begin(), s.end(), static_cast<char>(BasicField::Cell::Ship), '1');
                    std::replace(s.begin(), s.end(), static_cast<char>(BasicField::Cell::Water), '0');
                }
        );
    }

    size_t line_index = 0;

    auto result = std::accumulate(
            axis ? copy.begin() : copy.begin() + 1,
            copy.end(),
            axis ? "  ABCDEFGHIJ" : std::string(copy[0]),
            [&axis, &line_index](auto const & string, auto const & line)
            {
                return string + (axis ? "\n" + std::to_string(line_index++) + " " : "\n") + line;
            }
    );

    return result;
}


std::vector<BasicField::Position> BasicField::getSurroundings(
        unsigned char ship_size,
        bool is_horizontal,
        BasicField::Position const & position) const
{
    auto x_bound = static_cast<int>((is_horizontal ? ship_size : 1) + 1) + position.x;
    auto y_bound = static_cast<int>((is_horizontal ? 1 : ship_size) + 1) + position.y;

    std::vector<BasicField::Position> surroundings;

    for (int x = position.x - 1; x < x_bound; ++x)
    {
        for (int y = static_cast<int>(position.y) - 1; y < y_bound; ++y)
        {
            if (not Position::isValid(x, y))
            {
                continue;
            }
            if (not is_horizontal and x == position.x and y >= position.y and y < position.y + ship_size)
            {
                continue;
            }
            if (is_horizontal and y == position.y and x >= position.x and x < position.x + ship_size)
            {
                continue;
            }

            surroundings.emplace_back(static_cast<unsigned char>(x), static_cast<unsigned char>(y));
        }
    }

    return surroundings;
}


char & BasicField::operator[](
        BasicField::Position const & position)
{
    return this->at(position);
}


char BasicField::operator[](BasicField::Position const & position) const
{
    return this->at(position);
}


char & BasicField::at(
        BasicField::Position const & position)
{
    return this->field[static_cast<int>(position.y)][static_cast<int>(position.x) - 'A'];
}


char BasicField::at(
        BasicField::Position const & position) const
{
    return this->field[static_cast<int>(position.y)][static_cast<int>(position.x) - 'A'];
}


std::vector<std::string> BasicField::getLines() const
{
    return this->field;
}


std::vector<std::string> & BasicField::getField()
{
    return this->field;
}


std::vector<std::string> const & BasicField::getField() const
{
    return this->field;
}


BasicField::BasicField()
        :
        BasicField(BasicField::Cell::Water)
{}


BasicField::Position::Position(
        unsigned char x,
        unsigned char y)
        :
        x(x),
        y(y)
{
    if (static_cast<int>(x) >= BasicField::FieldSize + 'A'
        or static_cast<int>(x) < 'A'
        or static_cast<int>(y) < 0
        or static_cast<int>(y) >= BasicField::FieldSize)
    {
        std::stringstream err;

        err << "Position " << x << static_cast<unsigned int>(y) << " does not fit into the "
            << BasicField::FieldSize << "x" << BasicField::FieldSize
            << " field \n(allowed indices are A...J and 0...9)" << std::endl;

        throw std::out_of_range(err.str());
    }
}


bool BasicField::Position::operator!=(
        const BasicField::Position &other) const
{
    return this->x != other.x or this->y != other.y;
}


bool BasicField::Position::operator==(
        const BasicField::Position &other) const
{
    return not (*this != other);
}


bool BasicField::Position::operator<(
        const BasicField::Position &other) const
{
    return this->x - 'A' + this->y * 100 < other.x - 'A' + other.y * 100;
}


bool BasicField::Position::isValid(
        int x,
        int y)
{
    if (x < std::numeric_limits<unsigned char>::min() or y < std::numeric_limits<unsigned char>::min()
        or x > std::numeric_limits<unsigned char>::max() or y > std::numeric_limits<unsigned char>::max())
    {
        return false;
    }

    try
    {
        BasicField::Position(static_cast<unsigned char>(x), static_cast<unsigned char>(y));

        return true;
    }
    catch (...)
    {
        return false;
    }
}


BasicField::Position::Position()
        :
        x('A'),
        y(0)
{}


std::ostream & operator<<(
        std::ostream & os,
        BasicField::Position const & position)
{
    os << position.x << " " << static_cast<int>(position.y);

    return os;
}


std::vector<std::string> BasicField::repr() const
{
    decltype(repr()) result;

    result.reserve(11);

    result.emplace_back("  ABCDEFGHIJ");

    size_t idx = 0;

    for (auto const & line : this->getLines())
    {
        result.emplace_back(std::to_string(idx++) + " " + line);
    }

    return result;
}
