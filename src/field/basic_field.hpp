#ifndef BATTLESHIP_BASIC_FIELD_HPP
#define BATTLESHIP_BASIC_FIELD_HPP


#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <cstdint>


#include "utility/cppncurses/interface.hpp"


using cell_count_t = unsigned char;


class BasicField : public IRepresentable
{
public:
    static const size_t FieldSize = 10;

    enum Cell : char
    {
        FogOfWar = '?',
        Ship = '#',
        Water = '~',
        Hit = 'X',
        Miss = 'o',
        NearShip = '*'
    };

    class Position
    {
    public:
        static bool isValid(
                int x,
                int y);

        Position();
        Position(unsigned char x, unsigned char y);

        ~Position() = default;

        Position(Position const & other) = default;
        Position(Position && other) = default;

        Position & operator=(Position const & other) = default;
        Position & operator=(Position && other) = default;

        bool operator!=(Position const & other) const;
        bool operator==(Position const & other) const;
        bool operator<(Position const & other) const;

    public:
        unsigned char x;
        unsigned char y;
    };

    using ShipInfo = std::map<BasicField::Position, std::shared_ptr<std::vector<BasicField::Position>>>;

    static ShipInfo fillShipInfo(BasicField const & field, bool fill_info, bool & is_valid);

    BasicField();

    explicit BasicField(Cell filler_value);
    explicit BasicField(std::vector<std::string> field);

    virtual ~BasicField() = default;

    std::vector<BasicField::Position> getSurroundings(
            cell_count_t ship_size,
            bool is_horizontal,
            BasicField::Position const & position) const;

    virtual std::string toString(
            bool bin,
            bool axis) const;

    virtual std::vector<std::string> getLines() const;

    char & at(Position const & position);

    char at(Position const & position) const;

    BasicField(BasicField const & other) = default;
    BasicField(BasicField && other) noexcept = default;

    BasicField & operator=(BasicField const & other) = default;
    BasicField & operator=(BasicField && other) noexcept = default;

    char & operator[](Position const & position);

    char operator[](Position const & position) const;

    std::vector<std::string> repr() const override;

protected:
    std::vector<std::string> & getField();

    std::vector<std::string> const & getField() const;

private:
    std::vector<std::string> field;
};


std::ostream & operator<<(std::ostream & os, BasicField::Position const & position);


#endif //BATTLESHIP_BASIC_FIELD_HPP
