#include <algorithm>


#include "player_field.hpp"


PlayerField::PlayerField()
        :
        BasicField(BasicField::Cell::Water)
{}


PlayerField::PlayerField(
        std::vector<std::string> field)
        :
        BasicField(BasicField::Cell::Water),
        ships(PlayerField::ShipInfo())
{
    this->getField() = std::move(field);

    bool valid;

    this->ships = BasicField::fillShipInfo(*this, true, valid);

    if (not valid)
    {
        throw std::runtime_error("Error: invalid field");
    }
}


PlayerField::PlayerField(
        Cell filler_value)
        :
        BasicField(filler_value),
        ships(PlayerField::ShipInfo())
{}


const PlayerField::ShipInfo & PlayerField::getShipsInfo() const
{
    return this->ships;
}


std::pair<bool, cell_count_t> PlayerField::canPlace(
        cell_count_t ship_size,
        bool is_horizontal,
        BasicField::Position const & position,
        BasicField::Position const & top_left_corner,
        BasicField::Position const & bottom_right_corner) const
{
    if (position.x < top_left_corner.x or position.y < top_left_corner.y)
    {
        return {false, 0};
    }

    if (is_horizontal)
    {
        for (auto x = position.x; x < position.x + ship_size; ++x)
        {
            if (not Position::isValid(x, position.y)
                or (this->at({x, position.y}) != BasicField::Cell::Water)
                or x > bottom_right_corner.x)
            {
                return {false, 0};
            }
        }
    }
    else
    {
        for (auto y = position.y; y < position.y + ship_size; ++y)
        {
            if (not Position::isValid(position.x, y)
                or (this->at({position.x, y}) != BasicField::Cell::Water)
                or y > bottom_right_corner.y)
            {
                return {false, 0};
            }
        }
    }

    auto surroundings = this->getSurroundings(ship_size, is_horizontal, position);

    for (BasicField::Position const & cell : surroundings)
    {
        if (this->at(cell) == BasicField::Cell::Ship)
        {
            return {false, 0};
        }
    }

    return {
            true,
            std::count_if(
                    surroundings.begin(),
                    surroundings.end(),
                    [this](BasicField::Position const & p) -> bool
                    {
                        return this->at(p) == BasicField::Cell::Water;
                    }
            )
    };
}


void PlayerField::placeShip(
        cell_count_t ship_size,
        bool is_horizontal,
        BasicField::Position const & position)
{
    auto ship_cells = std::make_shared<std::vector<BasicField::Position>>();

    if (is_horizontal)
    {
        for (auto x = position.x; x < position.x + ship_size; ++x)
        {
            this->at({x, position.y}) = BasicField::Cell::Ship;

            ship_cells->emplace_back(x, position.y);
            //this->ships[{x, position.y}] = this->ships[position];
            this->ships.insert({{x, position.y}, ship_cells});
        }
    }
    else
    {
        for (auto y = position.y; y < position.y + ship_size; ++y)
        {
            this->at({position.x, y}) = BasicField::Cell::Ship;

            ship_cells->emplace_back(position.x, y);
            //this->ships[{position.x, y}] = this->ships[position];
            this->ships.insert({{position.x, y}, ship_cells});
        }
    }

    for (BasicField::Position const & cell : PlayerField::getSurroundings(ship_size, is_horizontal, position))
    {
        this->at(cell) = BasicField::Cell::NearShip;
    }
}
