#ifndef BATTLESHIP_PLAYER_FIELD_HPP
#define BATTLESHIP_PLAYER_FIELD_HPP


#include "field/basic_field.hpp"


class PlayerField : public BasicField
{
public:
    PlayerField();

    explicit PlayerField(BasicField::Cell filler_value);
    explicit PlayerField(std::vector<std::string> field);

    ~PlayerField() override = default;

    using ShipInfo = std::map<BasicField::Position, std::shared_ptr<std::vector<BasicField::Position>>>;

    void placeShip(
            cell_count_t ship_size,
            bool is_horizontal,
            BasicField::Position const & position);

    std::pair<bool, cell_count_t> canPlace(
            cell_count_t ship_size,
            bool is_horizontal,
            BasicField::Position const & position,
            BasicField::Position const & top_left_corner = {'A', 0},
            BasicField::Position const & bottom_right_corner = {'J', 9}) const;

    ShipInfo const & getShipsInfo() const;

private:
    ShipInfo ships;
};


#endif //BATTLESHIP_PLAYER_FIELD_HPP
