#include <getopt.h>


#include "parser.hpp"


static option long_options[] = {
        {"tournament", no_argument, nullptr, 'i'},
        {"sleep", required_argument, nullptr, 's'},
        {"wait", required_argument, nullptr, 'w'},
        {"help", no_argument, nullptr, 'h'},
        {"n", required_argument, nullptr, 'n'},
        {nullptr, 0, nullptr, 0}
};


GlobalArgs_t arg_parser::parse_arguments(
        int argc,
        char ** argv)
{
    GlobalArgs_t global_args = {};

    char const * opt_string = ":htn:s:";

    int arg;
    long long int int_buff;

    opterr = 0;

    while ((arg = getopt_long(argc, argv, opt_string, long_options, nullptr)) != -1)
    {
        switch (arg)
        {
            case 'n':
                try
                {
                    int_buff = std::stoll(optarg);
                }
                catch (std::exception & e)
                {
                    throw std::runtime_error("Cannot convert \"" + std::string(optarg) + "\" to integer");
                }
                if (int_buff < 0)
                {
                    throw std::runtime_error("Bad number of games: " + std::string(optarg));
                }
                global_args.multiple_games = true;
                global_args.n_games = int_buff;
                break;
            case 's':
                try
                {
                    int_buff = std::stoll(optarg);
                }
                catch (std::exception & e)
                {
                    throw std::runtime_error("Cannot convert \"" + std::string(optarg) + "\" to integer");
                }
                if (int_buff < 0)
                {
                    throw std::runtime_error("Bad sleep duration: " + std::string(optarg));
                }
                global_args.sleep = int_buff;
                break;
            case 'w':
                try
                {
                    int_buff = std::stoll(optarg);
                }
                catch (std::exception & e)
                {
                    throw std::runtime_error("Cannot convert \"" + std::string(optarg) + "\" to integer");
                }
                if (int_buff < 0)
                {
                    throw std::runtime_error("Bad wait duration: " + std::string(optarg));
                }
                global_args.wait = int_buff;
                break;
            case 't':
                global_args.tournament = true;
                break;
            case 'h':
                global_args.help = true;
                return global_args;
            case ':':
                throw std::runtime_error(std::string(argv[optind - 1]) + " is missing argument");
            case '?':
                throw std::runtime_error("Unknown option");
            default:
                throw std::runtime_error("Bad option: " + std::string(argv[optind - 1]));
        }
    }

    if (argv[optind] != nullptr)
    {
        global_args.exec1 = argv[optind];

        if (argv[optind + 1] != nullptr)
        {
            global_args.exec2 = argv[optind + 1];

            if (argv[optind + 2] != nullptr)
            {
                throw std::runtime_error(
                        "Extra non-option arguments: " + std::string(argv[optind + 2])
                        + "\n(this program takes only two non-option arguments");
            }
        }
        else
        {
            throw std::runtime_error(
                    "Missing required non-option argument EXECUTABLE_2");
        }
    }
    else
    {
        throw std::runtime_error(
                "Missing required non-option arguments EXECUTABLE_1, EXECUTABLE_2");
    }

    return global_args;
}