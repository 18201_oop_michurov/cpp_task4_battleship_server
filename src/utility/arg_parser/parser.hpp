#ifndef MAZE_PARSER_HPP
#define MAZE_PARSER_HPP


#include <iostream>


struct GlobalArgs_t
{
    std::string exec1;
    std::string exec2;
    size_t sleep = 35;
    size_t wait = 0;
    size_t n_games;
    bool multiple_games;
    bool tournament = false;
    bool help = false;
};


namespace arg_parser
{
    GlobalArgs_t parse_arguments(
            int argc,
            char ** argv);
}


#endif //MAZE_PARSER_HPP
