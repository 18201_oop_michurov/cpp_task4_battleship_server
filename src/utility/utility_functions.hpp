#ifndef BATTLESHIP_UTILITY_FUNCTIONS_HPP
#define BATTLESHIP_UTILITY_FUNCTIONS_HPP


namespace utility
{
    size_t randomIndex(
            size_t min_allowed_index,
            size_t max_allowed_index);
}


#endif //BATTLESHIP_UTILITY_FUNCTIONS_HPP
