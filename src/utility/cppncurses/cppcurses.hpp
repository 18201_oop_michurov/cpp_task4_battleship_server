#ifndef VISUALIZER_HPP
#define VISUALIZER_HPP


#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"


#include <ncurses.h>
#include <unordered_map>


#include "interface.hpp"


class Curses
{
    Curses();

    ~Curses();

public:
    Curses(Curses const & other) = delete;
    Curses(Curses && other) = delete;

    Curses & operator=(Curses const & other) = delete;
    Curses & operator=(Curses && other) = delete;

    Curses & enterCursesMode();

    Curses & exitCursesMode();

    static Curses & getInstance();

    enum class Attribute : unsigned int
    {
        Default = 0,
        Bold = A_BOLD,
        Underline = A_UNDERLINE,
        Dim = A_DIM,
        Standout = A_STANDOUT,
        Blink = A_BLINK
    };

    enum class Colour : unsigned int
    {
        Default = 8,
        Black = 1,
        Red = 2,
        Green = 3,
        Yellow = 4,
        Blue = 5,
        Magenta = 6,
        Cyan = 7,
        White = 8

    };

    enum class Visibility : unsigned int
    {
        Invisible = 0,
        Normal = 1,
        VeryVisible = 2
    };

    struct CharProperties
    {
        Colour colour = Colour::Default;
        Attribute attribute = Attribute::Default;
    };

    struct CursorPosition
    {
        int x = -1;
        int y = -1;
    };

    template <typename... Args>
    static Attribute combine(
            Attribute attr,
            Args... next);

    static Attribute combine(
            Attribute attr);

    Curses & putChar(
            int x,
            int y,
            unsigned char ch,
            Attribute attr = Attribute::Default,
            Colour col = Colour::Default);

    Curses & putString(
            int x,
            int y,
            std::string const & string,
            Attribute attr = Attribute::Default,
            Colour col = Colour::Default);

    Curses & displayRepresentable(
            int x,
            int y,
            IRepresentable const & representable,
            Attribute attr = Attribute::Default,
            Colour col = Colour::Default);

    Curses & setCursorPosition(
            int x,
            int y);

    Curses & setCursor(
            Visibility visibility);

    Curses & refresh();

    Curses & setAutoRefresh(bool flag);

    Curses & setCharProperties(
            char c,
            Attribute attr,
            Colour colour);

    Curses & setTerminalSize(
            int width,
            int height);

    Curses & clearLine(int y);

    Curses & clearAll();

    static CursorPosition getCursorPosition();

    static CursorPosition getBounds();

    static int getKey();

private:
    int last_attr;
    std::unordered_map<char, CharProperties> char_data;
    bool auto_refresh;
};


template <typename... Args>
Curses::Attribute Curses::combine(
        Curses::Attribute attr,
        Args... next)
{
    return static_cast<Curses::Attribute>(
            static_cast<unsigned int>(attr) | static_cast<unsigned int>(combine(next...))
    );
}


#pragma clang diagnostic pop

#endif //MAZE_VISUALIZER_HPP