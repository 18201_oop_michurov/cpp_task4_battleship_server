#ifndef SUBPROCESS_SUBPROCESS_HPP
#define SUBPROCESS_SUBPROCESS_HPP

#ifndef unix
#error "only works on UNIX"
#endif


#include <cstdio>
#include <ctime>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <cassert>
#include <wait.h>
#include <thread>


#include "../io/io.hpp"


namespace subprocess
{
    class not_a_child : public std::logic_error
    {
    public:
        not_a_child()
                :
                std::logic_error("")
        {}

        explicit not_a_child(std::string const & string)
                :
                std::logic_error(string)
        {}
    };


    class Subprocess
    {
        template <typename... Args>
        friend inline Subprocess spawn(Args... args);

        Subprocess(
                int stdin_fd,
                int stdout_fd,
                pid_t pid)
                :
                std_in(stdin_fd),
                std_out(stdout_fd),
                pid(pid),
                terminated(false)
        {}

    public:
        Subprocess(Subprocess && other) = default;
        Subprocess(Subprocess const & other) = delete;
        
        ~Subprocess();

        bool alive();

        // returns true if process exited within timeout after SIGTERM and returns false otherwise
        // if it didn't terminate within timeout, SIGKILL is sent
        bool terminate(std::chrono::milliseconds const & timeout = std::chrono::milliseconds(50));

        pid_t get_pid() const;

        Subprocess & operator=(Subprocess const & other) = delete;
        Subprocess & operator=(Subprocess && other) = delete;

    public:
        InputFileDescriptorWrapper std_out;
        OutputFileDescriptorWrapper std_in;

    private:
        pid_t pid;
        bool terminated;
    };


    template <typename... Args>
    inline Subprocess spawn(Args... args); // NOLINT
}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                                   Subprocess                                                       */
/* ------------------------------------------------------------------------------------------------------------------ */


inline subprocess::Subprocess::~Subprocess()
{
    close(this->std_in.fd);
    close(this->std_out.fd);

    if (this->alive())
    {
        this->terminate();
    }
}


inline bool subprocess::Subprocess::alive()
{
    if (this->terminated)
    {
        return false;
    }

    pid_t result = waitpid(this->pid, nullptr, WNOHANG);

    if (result == this->pid) // process with pid = this->pid has exited
    {
        waitpid(this->pid, nullptr, 0);
        this->terminated = true;

        this->pid = -10;

        return false;
    }
    else if (result == 0)
    {
        return true;
    }
    else
    {
        if (errno == ECHILD)
        {
            throw not_a_child(
                    "Process with pid = " + std::to_string(this->pid) + " is not a child of the caller process"
            );
        }

        throw std::runtime_error("waitpid() error. Check errno for further information");
    }
}


inline bool subprocess::Subprocess::terminate(
        std::chrono::milliseconds const & timeout)
{
    if (this->terminated)
    {
        return true;
    }

    close(this->std_in.fd);
    close(this->std_out.fd);

    kill(this->pid, SIGTERM);

    if (timeout.count() < 50)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    else
    {
        std::this_thread::sleep_for(timeout);
    }

    pid_t result = waitpid(this->pid, nullptr, WNOHANG);


    if (result == this->pid)
    {
        this->terminated = true;

        this->pid = -10;

        return true;
    }
    else if (result == 0)
    {
        kill(this->pid, SIGKILL); // kill it for good
        waitpid(this->pid, nullptr, 0); // harvest the zombie
        this->terminated = true;

        this->pid = -10;

        return false;
    }
    else
        // if didn't mess up with this->terminated value this branch will never be reached
        // but since i don't trust myself much, it's here, just in case
    {
        if (errno == ECHILD)
        {
            throw subprocess::not_a_child(
                    "Process with pid = " + std::to_string(this->pid) + " is not a child of the caller process"
            );
        }

        throw std::runtime_error("waitpid() error. Check errno for further information");
    }
}


inline pid_t subprocess::Subprocess::get_pid() const
{
    return this->pid;
}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                                      Spawn                                                         */
/* ------------------------------------------------------------------------------------------------------------------ */


template <typename... Args>
inline subprocess::Subprocess subprocess::spawn(Args... args)
{
    enum PipeFileDescriptors : unsigned int
    {
        READ_FD = 0,
        WRITE_FD = 1
    };

    int parentToChild[2];
    int childToParent[2];

    pid_t pid;

    // Behold: the unholy mix of C and C++

    assert(0 == pipe(parentToChild));
    assert(0 == pipe(childToParent));

    switch (pid = fork())
    {
        case -1:
            throw std::runtime_error("fork() failed");

        case 0: // Child
            assert(dup2(parentToChild[PipeFileDescriptors::READ_FD], STDIN_FILENO) != -1);
            assert(dup2(childToParent[PipeFileDescriptors::WRITE_FD], STDOUT_FILENO) != -1);
            assert(dup2(childToParent[PipeFileDescriptors::WRITE_FD], STDERR_FILENO) != -1);

            assert(close(parentToChild[PipeFileDescriptors::WRITE_FD]) == 0);
            assert(close(childToParent[PipeFileDescriptors::READ_FD]) == 0);

            execlp(args...);

            throw std::runtime_error("execlp() failed");
        default: // Parent
#ifdef DEBUG_OUTPUT
            std::cout << "Child " << pid << " process running..." << std::endl;
#endif

            assert(close(parentToChild[PipeFileDescriptors::READ_FD]) == 0);
            assert(close(childToParent[PipeFileDescriptors::WRITE_FD]) == 0);

            return {parentToChild[WRITE_FD], childToParent[READ_FD], pid};
    }
}


#endif //SUBPROCESS_SUBPROCESS_HPP
