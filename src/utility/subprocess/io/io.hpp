#ifndef SUBPROCESS_IO_HPP
#define SUBPROCESS_IO_HPP


#include <string>
#include <iostream>
#include <chrono>
#include <stdexcept>
#include <sstream>


class InputFileDescriptorWrapper;


namespace io
{
    class timeout_expired : public std::runtime_error
    {
    public:
        timeout_expired()
                :
                std::runtime_error("")
        {}

        explicit timeout_expired(
                std::string const & string)
                :
                std::runtime_error(string)
        {}
    };


    class write_error : public std::runtime_error
    {
    public:
        write_error()
                :
                std::runtime_error("")
        {}

        explicit write_error(
                std::string const & string)
                :
                std::runtime_error(string)
        {}
    };


    class end_of_file : public std::runtime_error
    {
    public:
        end_of_file()
                :
                std::runtime_error("")
        {}

        explicit end_of_file(
                std::string const & string)
                :
                std::runtime_error(string)
        {}
    };

    InputFileDescriptorWrapper & getline(
            InputFileDescriptorWrapper & is,
            std::string & string);

    InputFileDescriptorWrapper & getline(
            InputFileDescriptorWrapper & is,
            std::string & string,
            char delimiter);
}


class FileDescriptorWrapper
{
protected:
    explicit FileDescriptorWrapper(int fd);

public:
    FileDescriptorWrapper(FileDescriptorWrapper && other) = default;
    FileDescriptorWrapper(FileDescriptorWrapper const & other) = delete;

    FileDescriptorWrapper & operator=(FileDescriptorWrapper && other) = delete;
    FileDescriptorWrapper & operator=(FileDescriptorWrapper const & other) = delete;

public:
    int const fd;
};


class InputFileDescriptorWrapper : public FileDescriptorWrapper
{
    static inline std::string __read__(
            int fd,
            std::chrono::milliseconds const & timeout);

public:
    explicit InputFileDescriptorWrapper(int fd);

    bool eof() const;

    bool timeout_expired() const;

    void set_timeout(std::chrono::milliseconds const & ms);

    std::string read(std::chrono::milliseconds const & timeout);

    std::string read();

    std::string getline();

    std::string getline(char delimiter);

    template <typename T>
    friend inline InputFileDescriptorWrapper & operator>>(
            InputFileDescriptorWrapper & is,
            T & val);

    operator bool() const noexcept;

    void clear();

private:
    std::stringstream is;
    std::chrono::milliseconds default_timeout;
    bool m_eof;
    bool m_timeout_expired;
};


class OutputFileDescriptorWrapper : public FileDescriptorWrapper
{
public:
    explicit OutputFileDescriptorWrapper(int fd);

    int write(std::string const & string);

    friend OutputFileDescriptorWrapper & operator<<(
            OutputFileDescriptorWrapper & is,
            std::basic_ostream<char> & (*pf)(std::basic_ostream<char> &));

    template <typename T>
    friend OutputFileDescriptorWrapper & operator<<(
            OutputFileDescriptorWrapper & is,
            T const & val);


private:
    std::stringstream ss;
};


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                               FileDescriptorWrapper                                                */
/* ------------------------------------------------------------------------------------------------------------------ */


inline FileDescriptorWrapper::FileDescriptorWrapper(
        int fd)
        :
        fd(fd)
{}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                                       io                                                           */
/* ------------------------------------------------------------------------------------------------------------------ */


inline InputFileDescriptorWrapper & io::getline(
        InputFileDescriptorWrapper & is,
        std::string & string)
{
    string = is.getline();

    return is;
}


inline InputFileDescriptorWrapper & io::getline(
        InputFileDescriptorWrapper & is,
        std::string & string,
        char delimiter)
{
    string = is.getline(delimiter);

    return is;
}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                             InputFileDescriptorWrapper                                             */
/* ------------------------------------------------------------------------------------------------------------------ */


inline InputFileDescriptorWrapper::InputFileDescriptorWrapper(
        int fd)
        :
        FileDescriptorWrapper(fd),
        m_eof(false),
        m_timeout_expired(false),
        default_timeout(std::chrono::milliseconds(1000)),
        is()
{}


inline std::string InputFileDescriptorWrapper::__read__(
        int fd,
        std::chrono::milliseconds const & timeout)
{
    timeval tv = {};
    tv.tv_sec = timeout.count() / 1000;
    tv.tv_usec = (timeout.count() % 1000) * 1000;

    const size_t BUFF_SIZE = 1024;
    ssize_t read_size;

    char buff[BUFF_SIZE + 1] = {};

    std::string result;

    int retval;

    fd_set rfds;

    FD_ZERO(&rfds);
    FD_SET(fd, &rfds); // NOLINT(hicpp-signed-bitwise)

    retval = select(1 + fd, &rfds, nullptr, nullptr, &tv);
    switch (retval)
    {
        case -1:
            throw std::runtime_error("select()");
        case 0:
            throw io::timeout_expired("Timeout expired");
        case 1:
            read_size = ::read(fd, buff, BUFF_SIZE);

            switch (read_size)
            {
                case 0:
                    throw io::end_of_file();
                case -1:
                    if (errno == EINTR or errno == EAGAIN)
                    {
                        errno = 0;
                        break;
                    }
                    else
                    {
                        throw std::runtime_error("read() failed");
                    }
                default:
                    result.append(buff, read_size);
                    if (read_size < BUFF_SIZE)
                    {
                        return result;
                    }
            }

            do
            {
                read_size = ::read(fd, buff, BUFF_SIZE);

                switch (read_size)
                {
                    case 0:
                        return result;
                    case -1:
                        if (errno == EINTR or errno == EAGAIN)
                        {
                            errno = 0;
                            break;
                        }
                        else
                        {
                            throw std::runtime_error("read() failed");
                        }
                    default:
                        result.append(buff, read_size);
                        if (read_size < BUFF_SIZE)
                        {
                            return result;
                        }
                }

                tv.tv_sec = 0;
                tv.tv_usec = 0;

                FD_ZERO(&rfds);
                FD_SET(fd, &rfds); // NOLINT(hicpp-signed-bitwise)
            }
            while ((retval = select(1 + fd, &rfds, nullptr, nullptr, &tv)) == 1);

            if (retval == -1)
            {
                throw std::runtime_error("select()");
            }
            return result;
        default:
            throw std::logic_error("critical read() error");
    }
}


inline std::string InputFileDescriptorWrapper::read(
        std::chrono::milliseconds const & timeout)
{
    if (this->m_eof)
    {
        throw io::end_of_file();
    }

    try
    {
        return __read__(this->fd, timeout);
    }
    catch (io::end_of_file & e)
    {
        this->m_eof = true;
        throw e;
    }
    catch (io::timeout_expired & e)
    {
        this->m_timeout_expired = true;
        throw e;
    }
}


template <typename T>
inline InputFileDescriptorWrapper & operator>>(
        InputFileDescriptorWrapper & is,
        T & val)
{
    try
    {
        is.is >> val;

        if (is.is.eof())
        {
            is.is.str(is.read());
            is.is.clear();

            is.is >> val;
        }
    }
    catch (io::end_of_file &)
    {}
    catch (io::timeout_expired &)
    {}

    return is;
}


template <>
inline InputFileDescriptorWrapper & operator>>(
        InputFileDescriptorWrapper & is,
        std::string & val)
{
    try
    {
        is.is >> val;

        if (is.is.tellg() == -1)
        {
            is.is.str(is.read());
            is.is.clear();

            is.is >> val;
        }
    }
    catch (io::end_of_file &)
    {}
    catch (io::timeout_expired &)
    {}

    return is;
}


inline std::string InputFileDescriptorWrapper::getline()
{
    std::string buff;

    try
    {
        if (/*buff.empty()*/ this->is.eof() or this->is.str().empty() or this->is.tellg() == this->is.str().size())
        {
            this->is.str(this->read());
            this->is.clear();
        }

        this->is.sync();

        std::getline(this->is, buff);
    }
    catch (io::end_of_file &)
    {}
    catch (io::timeout_expired &)
    {}

    return buff;
}


inline std::string InputFileDescriptorWrapper::getline(
        char delimiter)
{
    std::string buff;

    try
    {
        if (/*buff.empty()*/ this->is.eof() or this->is.str().empty() or this->is.tellg() == this->is.str().size()) // fix condition (test with delimiter = ' ')
        {
            this->is.str(this->read());
            this->is.clear();
        }

        std::getline(this->is, buff, delimiter);
    }
    catch (io::end_of_file &)
    {}
    catch (io::timeout_expired &)
    {}

    return buff;
}


inline void InputFileDescriptorWrapper::set_timeout(
        std::chrono::milliseconds const & ms)
{
    this->default_timeout = ms;
}


inline std::string InputFileDescriptorWrapper::read()
{
    return this->read(this->default_timeout);
}


inline InputFileDescriptorWrapper::operator bool() const noexcept
{
    return not this->m_eof and not this->m_timeout_expired;
}


inline bool InputFileDescriptorWrapper::eof() const
{
    return this->m_eof;
}


inline bool InputFileDescriptorWrapper::timeout_expired() const {
    return this->m_timeout_expired;
}


inline void InputFileDescriptorWrapper::clear()
{
    this->m_timeout_expired = false;
    this->m_eof = false;
}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                            OutputFileDescriptorWrapper                                             */
/* ------------------------------------------------------------------------------------------------------------------ */


inline OutputFileDescriptorWrapper & operator<<(
        OutputFileDescriptorWrapper & is,
        std::basic_ostream<char> & (*pf)(std::basic_ostream<char> &))
{
    pf(is.ss);

    if (not is.ss.str().empty())
    {
        if (is.write(is.ss.str()) < 0)
        {
            throw io::write_error("write() failed");
        }
    }

    is.ss.str("");

    return is;
}


template <typename T>
inline OutputFileDescriptorWrapper & operator<<(
        OutputFileDescriptorWrapper & is,
        T const & val)
{
    is.ss << val;

    return is;
}


inline OutputFileDescriptorWrapper::OutputFileDescriptorWrapper(
        int fd)
        :
        FileDescriptorWrapper(fd)
{}


inline int OutputFileDescriptorWrapper::write(std::string const & string)
{
    return ::write(this->fd, string.c_str(), string.size());
}


#endif //SUBPROCESS_IO_HPP
