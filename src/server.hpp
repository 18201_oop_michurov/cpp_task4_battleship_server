#ifndef SERVER_SERVER_HPP
#define SERVER_SERVER_HPP


#include <chrono>
#include "field/basic_field.hpp"


namespace server
{
    std::pair<std::string, std::string> makeNames(
            std::string const & exec1,
            std::string const & exec2);

    // return winner name (first) and executable name (second)
    std::pair<std::string, std::string> runBattle(
            std::string const & exec1,
            std::string const & exec2,
            std::chrono::milliseconds const & delay,
            std::chrono::milliseconds const & wait_between_starting_exec,
            std::string const &title);
}


#endif //SERVER_SERVER_HPP
