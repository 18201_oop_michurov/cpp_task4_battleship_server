#include <unordered_map>
#include <thread>


#include "server.hpp"
#include "utility/cppncurses/cppcurses.hpp"
#include "utility/arg_parser/parser.hpp"


static inline void printHelp()
{
    static char const * help =
            R"=(Usage: server [KEYS] {EXECUTABLE_1} {EXECUTABLE_2}
Runs battleship games between EXECUTABLE_1 and EXECUTABLE_2.

Allowed keys:
    -s, --sleep=DURATION       set delay between turns in ms (35 ms by default)

    -w, --wait=DURATION        set delay between starting executables in ms
                               (0 ms by default)
                               (set to about 1200 for programs that use C-style
                               random number generation)

    -n=NUMBER_OF_GAMES         set number of games between competitors, switching
                               sides after each. If this number is odd,
                               it's increased dy one.

    -h, --help                 display help (the one you're reading right now)

Non-option parameters:
    EXECUTABLE_1               (required) path to first executable

    EXECUTABLE_2               (required) path to second executable)=";
    std::cout << help << std::endl;
}


int main(
        int argc,
        char * argv[])
{
    GlobalArgs_t global_args = {};

    try
    {
        global_args = arg_parser::parse_arguments(argc, argv);
    }
    catch (std::exception & e)
    {
        std::cout << "Error: " << e.what() << std::endl;

        printHelp();

        exit(EXIT_FAILURE);
    }

    if (global_args.help)
    {
        printHelp();

        exit(EXIT_SUCCESS);
    }

    auto & terminal = Curses::getInstance();

    if (not global_args.multiple_games)
    {
        server::runBattle(
                global_args.exec1,
                global_args.exec2,
                std::chrono::milliseconds(global_args.sleep),
                std::chrono::milliseconds(global_args.wait),
                "EPIC GAMER MOMENT"
        );

        terminal.clearLine(21);
        terminal.putString(
                0, 21,
                "Press any key to continue. . .",
                Curses::combine(
                        Curses::Attribute::Blink,
                        Curses::Attribute::Bold
                )
        );

        Curses::getKey();
        terminal.exitCursesMode();
    }
    else
    {
        std::unordered_map<std::string, size_t> statistics = {};

        if (global_args.n_games % 2)
        {
            global_args.n_games += 1;
            std::cout << "Making number of games even (" << global_args.n_games << ")" << std::endl;
        }

        std::string p1 = global_args.exec1;
        std::string p2 = global_args.exec2;

        auto names = server::makeNames(p1, p2);

        names.first += " (" + p1 + ")";
        names.second += " (" + p2 + ")";

        for (size_t i = 0; i < global_args.n_games; ++i)
        {
            terminal.clearAll();

            auto winner = server::runBattle(
                    p1,
                    p2,
                    std::chrono::milliseconds(global_args.sleep),
                    std::chrono::milliseconds(global_args.wait),
                    "Game " + std::to_string(i + 1) + " / " + std::to_string(global_args.n_games) + "\n"
            );

            statistics[winner.first + " (" + winner.second + ")"] += 1;

            std::swap(p1, p2);

            terminal.putString(0, 21, "Switching sides...");
            terminal.refresh();

            std::this_thread::sleep_for(std::chrono::milliseconds(0));
        }

        terminal.clearAll();
        terminal.setCursorPosition(0, 0);

        terminal.putString(0, 0, "Results:", Curses::Attribute::Bold);
        terminal.putString(0, 1, names.first + " : " + std::to_string(statistics[names.first]));
        terminal.putString(0, 2, names.second + " : " + std::to_string(statistics[names.second]));

        terminal.putString(
                0, 4,
                "Press any key to continue. . .",
                Curses::combine(
                        Curses::Attribute::Blink,
                        Curses::Attribute::Bold
                )
        );

        Curses::getKey();
        terminal.exitCursesMode();
    }

    return 0;
}

